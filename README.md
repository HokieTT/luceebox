# LuceeBox

## builds **docker** image with **lucee** war install running via **jetty-runner** using **oracle java 8** using **busybox**
---


```
#!bash

> docker build --rm -t sherrala/luceebox .
> docker run --rm -ti --name luceebox sherrala/luceebox
> docker ps
> docker inspect whateverthecontaineridis
// find the IPAddress key/value...it will look like "IPAddress": "172.17.0.18",
> curl http://<IPAddress>:8080
```
