FROM progrium/busybox
MAINTAINER Sean Herrala version: 0.1
RUN opkg-install wget
RUN ( wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" -O /tmp/jre.tar.gz http://download.oracle.com/otn-pub/java/jdk/8u40-b26/jre-8u40-linux-x64.tar.gz && gunzip /tmp/jre.tar.gz && cd /opt && tar xf /tmp/jre.tar && rm /tmp/jre.tar)
RUN ln -sf /lib/libpthread-2.18.so /lib/libpthread.so.0
RUN ln -s /opt/jre1.8.0_40/bin/java /usr/bin/java
RUN mkdir /opt/lucee
RUN ( wget --no-check-certificate -O /opt/lucee/jetty-runner.jar http://central.maven.org/maven2/org/eclipse/jetty/jetty-runner/9.2.9.v20150224/jetty-runner-9.2.9.v20150224.jar)
RUN ( wget --no-check-certificate -O /opt/lucee/lucee.war http://bitbucket.org/lucee/lucee/downloads/lucee-4.5.1.000.war)
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/opt/lucee/jetty-runner.jar", "/opt/lucee/lucee.war"]

